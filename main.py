import graphics
import track_optimisation
import track_gen

if __name__ == '__main__':
    eps = 0.001
    n = 100
    L, l, y, F_max = [float(inp) for inp in input().split()]
    tau_array = [i/n for i in range(n + 1)]
    coord_array = track_gen.track_generation(tau_array, L, l, y)
    velocity_array = track_optimisation.find_opt_track_vel(tau_array, coord_array, F_max)
    graphics.draw_track(tau_array, coord_array, velocity_array, l, L, y, F_max)
