'''Alex'''

import track_optimisation
from enum import Enum
from math import ceil, factorial, sqrt

eps = 0.001


def get_distance(p1, p2):
    return sqrt((p2[0] - p1[0]) ** 2 + (p2[1] - p1[1]) ** 2)


class Direction(Enum):
    LEFT = 0
    RIGHT = 1


def gen_bezier_n_ord(tau_array, curve_ord, points):
    result = [(0, 0) for _ in tau_array]
    for (tau_num, tau) in enumerate(tau_array):
        for pwr in range(curve_ord + 1):
            result[tau_num] = (result[tau_num][0] + factorial(curve_ord) / factorial(pwr) / factorial(curve_ord - pwr) * points[pwr][0] * tau ** pwr * (1 - tau) ** (curve_ord - pwr), result[tau_num][1] + factorial(curve_ord) / factorial(pwr) / factorial(curve_ord - pwr) *points[pwr][1] * tau ** pwr * (1 - tau) ** (curve_ord - pwr))
    return result


def check_gate(gate_coords, direc, tau_array, coords_array):
    """works only at the big number of len(coords_array)"""
    global eps
    if coords_array[0][0] > gate_coords[1]:
        return True
    if coords_array[len(coords_array) - 1][0] < gate_coords[1]:
        return True
    upside = - 1
    for i, coords in enumerate(coords_array):
        if (gate_coords[1] - coords_array[i][1]) * upside > 0:
            upside = upside * -1
            if direc == Direction.LEFT and (coords[0] + coords_array[i - 1][0]) / 2 < gate_coords[0] - eps:
                return False
            elif direc == Direction.RIGHT and (coords[0] + coords_array[i - 1][0]) / 2 > gate_coords[0] + eps:
                return False
    return True


def cut_tau_array_in_interval(tau_array, left, right):
    length = len(tau_array)
    if length == 0:
        return []
    if length == 1:
        if left < tau_array[0] < right:
            return tau_array
        else:
            return []
    left_int = int(left * (length - 1))
    right_int = int(right * (length - 1))
    return tau_array[left_int: right_int + 1]


def scale_cut_to_full(cut_tau_array):
    if not cut_tau_array:
        return []
    if len(cut_tau_array) == 1:
        return cut_tau_array
    new_step = 1 / (len(cut_tau_array) - 1)
    return [i * new_step for i, _ in enumerate(cut_tau_array)]


def smart_join_coords(*coord_arrays):
    if not coord_arrays:
        return []
    res = coord_arrays[0]
    for arr in coord_arrays[1:]:
        if not arr:
            continue
        if res[len(res) - 1] == arr[0]:
            res += arr[1:]
        else:
            res += arr
    return res


def generate_polyline(tau_array, L, l, y):
    tau_start = 0
    tau_gate1 = 2 * (1 + l) / 2 / (1 + 2 * l)
    tau_center = 1
    start = (0, 0)
    gate1 = ((1 + l) / 2, (1 - L) / 2)
    center = (1 / 2, 1 / 2)
    start_gate1 = gen_bezier_n_ord(scale_cut_to_full(cut_tau_array_in_interval(tau_array, tau_start, tau_gate1)),
                                   1, [start, gate1])
    gate1_center = gen_bezier_n_ord(scale_cut_to_full(cut_tau_array_in_interval(tau_array, tau_gate1, tau_center)),
                                   1, [gate1, center])
    return smart_join_coords(start_gate1, gate1_center)


def generate_polyline_custom_points(tau_array, start, turn, finish):
    start_turn_length = get_distance(finish, turn)
    turn_finish_length = get_distance(turn, finish)
    tau_start = 0
    tau_turn = start_turn_length / (start_turn_length + turn_finish_length)
    tau_finish = 1
    start_turn = gen_bezier_n_ord(scale_cut_to_full(cut_tau_array_in_interval(tau_array, tau_start, tau_turn)),
                                   1, [start, turn])
    turn_center = gen_bezier_n_ord(scale_cut_to_full(cut_tau_array_in_interval(tau_array, tau_turn, tau_finish)),
                                   1, [turn, finish])
    return smart_join_coords(start_turn, turn_center)


def gen_simple_2_ord_bezier_trajectory(tau_array, L, l, y):
    gate1_right = ((1 + l + y) / 2, (1 - L) / 2)
    return gen_bezier_n_ord(tau_array, 2, [(0, 0), gate1_right, (1 / 2, 1 / 2)])


def find_interim_tau_inds(tau_array, polyline, right_cutoff_x_coord):
    "Find the pair of taus for the most right points of polyline, if there are no any, exception is thrown"
    if len(polyline) < 6:
        raise AttributeError('Insufficient number of points in polyline')
    x_diff_lower = polyline[1][0] - polyline[0][0]
    x_diff_higher = polyline[-2][0] - polyline[-1][0]
    tau_ind1 = int((right_cutoff_x_coord - polyline[0][0]) / x_diff_lower)
    tau_ind2 = len(tau_array) - ceil((right_cutoff_x_coord - polyline[-1][0]) / x_diff_higher) - 1
    if polyline[tau_ind1][0] > right_cutoff_x_coord:
        raise AttributeError('polyline has improper vertex')
    return tau_ind1, tau_ind2


def gen_3_ord_bezier_partial(tau_array, L, l, y, tau_first=0, tau_last=1/2, vertex_x=1):
    gate1_left = ((1 + l - y) / 2, (1 - L) / 2)
    gate1_right = ((1 + l + y) / 2, (1 - L) / 2)
    tau_step = tau_array[1] - tau_array[0]
    first_ind = ceil(tau_first / tau_step)
    last_ind = int(tau_last / tau_step)
    poly_aux = generate_polyline_custom_points(tau_array, (0, 0), (vertex_x, gate1_right[1]), (1 / 2, 1 / 2))
    linear_part1 = poly_aux[: first_ind + 1]
    linear_part2 = poly_aux[last_ind:]
    bez_point1 = linear_part1[-1]
    bez_point2 = linear_part2[0]
    try:
        interim_tau_inds = find_interim_tau_inds(tau_array, poly_aux, vertex_x)
        bezier_part = gen_bezier_n_ord(scale_cut_to_full(cut_tau_array_in_interval(tau_array, tau_array[first_ind],
                                                                                   tau_array[last_ind])), 3,
                                       [bez_point1, poly_aux[interim_tau_inds[0]],
                                        poly_aux[interim_tau_inds[1]], bez_point2])
    except AttributeError:
        bezier_part = []
    return smart_join_coords(linear_part1, bezier_part, linear_part2)

def reflect_center_symmetrically(tau_array, coords_array):
    res = coords_array.copy()
    return res[:-2:2] + [(1 - i[0], 1 - i[1]) for i in res[::2][::-1]]


def try_3_ord_bezier(tau_array, L, l, y):
    gate1_left = ((1 + l - y) / 2, (1 - L) / 2)
    gate1_right = ((1 + l + y) / 2, (1 - L) / 2)
    gate2_left = ((1 - l - y) / 2, (1 + L) / 2)
    gate2_right = ((1 - l + y) / 2, (1 + L) / 2)
    vertex_x_array = [(1.1) ** i * (gate1_left[0] + 2 * eps) for i in range(15)]
    best_solution = ()
    for vertex_x in vertex_x_array:
        poly_aux = generate_polyline_custom_points(tau_array, (0, 0), (vertex_x, gate1_right[1]), (1 / 2, 1 / 2))
        interim_tau_inds = find_interim_tau_inds(tau_array, poly_aux, vertex_x)
        tau_start_array = [i / 10 * tau_array[interim_tau_inds[0]] for i in range(10)]
        tau_end_array = [tau_array[interim_tau_inds[1]] + i / 10 * (1 - tau_array[interim_tau_inds[1]]) for i in range(10)]
        for tau_start in tau_start_array:
            for tau_end in tau_end_array:
                if tau_start > tau_end:
                    raise ValueError("tau_start exceeds tau_end")
                coords_array_part = gen_3_ord_bezier_partial(tau_array, L, l, y, tau_start,
                                                             tau_end, vertex_x)
                coords_array = reflect_center_symmetrically(tau_array, coords_array_part)
                time = track_optimisation.find_opt_track_time(tau_array, coords_array, 10)
                if not check_gate(gate1_left, Direction.LEFT, tau_array, coords_array):
                    continue
                if not check_gate(gate1_right, Direction.RIGHT, tau_array, coords_array):
                    continue
                if not best_solution:
                    best_solution = (time, coords_array)
                    continue
                if time < best_solution[0]:
                    best_solution = (time, coords_array)
    return best_solution[1]


def track_generation(tau_array, L, l, y):
    gate1_left = ((1 + l - y) / 2, (1 - L) / 2)
    gate1_right = ((1 + l + y) / 2, (1 - L) / 2)
    gate2_left = ((1 - l - y) / 2, (1 + L) / 2)
    gate2_right = ((1 - l + y) / 2, (1 + L) / 2)
    print(gate1_left, gate1_right, gate2_left, gate2_right)
    coords_array = try_3_ord_bezier(tau_array, L, l, y)
    print(coords_array)
    return coords_array
